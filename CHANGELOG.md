# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.3.9](https://gitlab.com/jhechtf/jhechtf/compare/v1.3.8...v1.3.9) (2023-04-29)


### Miscellaneous

- Ci: Updates asset stuff ([84ba4dc](https://gitlab.com/jhechtf/jhechtf/commit/84ba4dc146de134d2ce033abeeccaea77a3c04d0))
- Merge branch 'ci/dammit-scuba-steve' into 'master' ([5e873da](https://gitlab.com/jhechtf/jhechtf/commit/5e873dadc807031f7c75286bcefda4fb95dc6b57))

## [1.3.8](https://gitlab.com/jhechtf/jhechtf/compare/v1.3.7...v1.3.8) (2023-04-29)


### Miscellaneous

- Ci: fixes build command ([b70d17e](https://gitlab.com/jhechtf/jhechtf/commit/b70d17e455962725948754936ff78a6324fc3889))
- Merge branch 'ci/rename-build-command' into 'master' ([d5204f5](https://gitlab.com/jhechtf/jhechtf/commit/d5204f5ac927668dbd49b6cc17d87c816497f954))

## [1.3.7](https://gitlab.com/jhechtf/jhechtf/compare/v1.3.6...v1.3.7) (2023-04-29)


### Miscellaneous

- Feat: Move to Astro ([d4667b3](https://gitlab.com/jhechtf/jhechtf/commit/d4667b3b132377b45dc8dd88d95dc74d01c03e98))
- Merge branch 'feat/switch-to-astro' into 'master' ([41ac5e4](https://gitlab.com/jhechtf/jhechtf/commit/41ac5e485c71a23fb59fbe169cc7bb9fa45ac051)), closes [#12](https://gitlab.com/jhechtf/jhechtf/issues/12/

## [1.3.6](https://gitlab.com/jhechtf/jhechtf/compare/v1.3.5...v1.3.6) (2022-11-27)


### Miscellaneous

- Chore: Adds in FullStory tracking ([c95e120](https://gitlab.com/jhechtf/jhechtf/commit/c95e1209af5229c381ca8c17c3852bcb9d0e4a81))

## [1.3.5](https://gitlab.com/jhechtf/jhechtf/compare/v1.3.4...v1.3.5) (2020-06-08)


### Bug Fixes

- Fixes bug on contacts page ([5d65db5](https://gitlab.com/jhechtf/jhechtf/commit/5d65db59993c5d05da90357d7aa1acc96ea4df3e)), closes [#7](https://gitlab.com/jhechtf/jhechtf/issues/7/


### Miscellaneous

- Removes previous changes to the gitlab-ci ([3a1b973](https://gitlab.com/jhechtf/jhechtf/commit/3a1b973d5d3c270faed4089753b083b6b07bafe6)), closes [#6](https://gitlab.com/jhechtf/jhechtf/issues/6/
- Merge branch 'fix/contacts-page-layout' into 'master' ([e2eac75](https://gitlab.com/jhechtf/jhechtf/commit/e2eac751e347edb3df5497094e0dbcb3ed117f71)), closes [#7](https://gitlab.com/jhechtf/jhechtf/issues/7/

## [1.3.4](https://gitlab.com/jhechtf/jhechtf/compare/v1.3.3...v1.3.4) (2020-06-07)


### Miscellaneous

- Debugging commit ([1c9fde6](https://gitlab.com/jhechtf/jhechtf/commit/1c9fde6bf271f275322e07fc7390a3c41b9e261f))

## [1.3.3](https://gitlab.com/jhechtf/jhechtf/compare/v1.3.2...v1.3.3) (2020-06-07)


### Miscellaneous

- Forgot to use double ampersands when creating the config directory ([df43521](https://gitlab.com/jhechtf/jhechtf/commit/df435218ad466ab5f570f4c990e141cc76c14fb2)), closes [#6](https://gitlab.com/jhechtf/jhechtf/issues/hopefully resolves #6/

## [1.3.2](https://gitlab.com/jhechtf/jhechtf/compare/v1.3.1...v1.3.2) (2020-06-07)


### Miscellaneous

- Updated build to use config files ([f7194cf](https://gitlab.com/jhechtf/jhechtf/commit/f7194cfab58a66d8d774f41d41addc882d76c4de))

## [1.3.1](https://gitlab.com/jhechtf/jhechtf/compare/v1.3.0...v1.3.1) (2020-06-07)


### Miscellaneous

- Update yarn lock and CI for deployment ([704f651](https://gitlab.com/jhechtf/jhechtf/commit/704f651494f3a53bfd4c8efc947f2ca9eaf99dd1))
- Merge branch 'master' of https://gitlab.com/jhechtf/jhechtf ([daeabea](https://gitlab.com/jhechtf/jhechtf/commit/daeabeaf138d73106623cd3f5af4a09d38d4960a))
- Merge branch 'move-to-nuxt' into 'master' ([a9186cc](https://gitlab.com/jhechtf/jhechtf/commit/a9186cc7be5036ec7525b4110198e0bdde919d08)), closes [#5](https://gitlab.com/jhechtf/jhechtf/issues/5/

# [1.3.0](https://gitlab.com/jhechtf/jhechtf/compare/v1.2.0...v1.3.0) (2020-06-07)


### Features

- Added in new Contact page ([c17fe45](https://gitlab.com/jhechtf/jhechtf/commit/c17fe45d3b455822cab5bbfea606a74e0886dc6f))


### Bug Fixes

- Fixed contact me form bug ([8eb64ca](https://gitlab.com/jhechtf/jhechtf/commit/8eb64cafb9382f13b3208181a5d70de2d8b9a214))


### Miscellaneous

- Adds in PurgeCSS ([2948a0a](https://gitlab.com/jhechtf/jhechtf/commit/2948a0a09a2052d2c5888c74b48cc51260362a8b))
- 📚(README): Updates the readme ([3f40f64](https://gitlab.com/jhechtf/jhechtf/commit/3f40f64ad36f0c9bf0e02223274645f54fee2e6e))
- Added in .env files ([0abc791](https://gitlab.com/jhechtf/jhechtf/commit/0abc791b00861ecf78c696f71653d0d6dfc95782))
- Í²¥ Moves codebase to Nuxt ([25b9720](https://gitlab.com/jhechtf/jhechtf/commit/25b97200ab501dc44b7e3a090c3b1f76d454f14a)), closes [#5](https://gitlab.com/jhechtf/jhechtf/issues/5/
- Merge branch 'feat/add-in-contact-page' into 'master' ([7b8cce8](https://gitlab.com/jhechtf/jhechtf/commit/7b8cce89398a4b158a27fe0dbf9206c9bf6bfb52))
- Merge branch 'feat/add-in-contact-page' into 'master' ([afb860a](https://gitlab.com/jhechtf/jhechtf/commit/afb860a26832af9b3fe525952dec4f3be4a471bc))
- Merge branch 'master' of https://gitlab.com/jhechtf/jhechtf ([cb1c83d](https://gitlab.com/jhechtf/jhechtf/commit/cb1c83d5a2766091da25cb8dfdacef870f8bc387))
- Merge branch 'move-to-nuxt' into 'master' ([11b0667](https://gitlab.com/jhechtf/jhechtf/commit/11b0667f544e86aa7714362fd48dd9633d518a1a)), closes [#5](https://gitlab.com/jhechtf/jhechtf/issues/5/

# [1.2.0](https://gitlab.com/jhechtf/jhechtf/compare/v1.1.0...v1.2.0) (2020-01-16)


### Features

- Adds in Google Analytics ([726a06b](https://gitlab.com/jhechtf/jhechtf/commit/726a06b0c4f868b69002983576d9540b46f74f9a))


### Miscellaneous

- Update public path ([4b37d59](https://gitlab.com/jhechtf/jhechtf/commit/4b37d591fe6bd2102b2e9da23baad47dc69f7c1e))
- Merge branch 'config/update-public-path' into 'master' ([a93d76f](https://gitlab.com/jhechtf/jhechtf/commit/a93d76f78c2e153fb6e3dca27e20ffc1dcce759f))

# 1.1.0 (2020-01-07)


### Features

- Finalized design of home page. ([a955826](https://gitlab.com/jhechtf/jhechtf/commit/a955826eff691ca26d77255a0b2f4037668a1a91))


### Bug Fixes

- Fixed card deck display on tablet and smaller devices ([f7b2f69](https://gitlab.com/jhechtf/jhechtf/commit/f7b2f69cc82f06e6126c8bc770cbe55cc3b98ac3))
- Fixed router error for production deployments ([1629e52](https://gitlab.com/jhechtf/jhechtf/commit/1629e52d3bea631dca2f5c0c14bf0898c62fe7a3))


### Miscellaneous

- :construction: Random Update ([a0cf6bf](https://gitlab.com/jhechtf/jhechtf/commit/a0cf6bff21a1776ebcd2dcb3f5a8150c35f4aed6))
- :tada: Initial Commit ([2f9f41a](https://gitlab.com/jhechtf/jhechtf/commit/2f9f41a22ddbda4a02cdbc07e6b1e9df2e09443a))
- General Update -- updated packages and dependencies ([35ac53d](https://gitlab.com/jhechtf/jhechtf/commit/35ac53df1b63bf95e88d74fb4de41208e6f208f3))
- Update includes a lot of combined local work ([c30589b](https://gitlab.com/jhechtf/jhechtf/commit/c30589bde6c7c538972262bb59a648edf116853a))
- Added in public artifacts path for deploy to pages. ([7536526](https://gitlab.com/jhechtf/jhechtf/commit/7536526a8fe2803591bcdc78602505e9311f7472))
- Fixed an error in the pages display pipeline ([59ed7cf](https://gitlab.com/jhechtf/jhechtf/commit/59ed7cfd6e1105885be2af158e115b784ddad7f3))
- Fixed deployment to gitlab pages ([57ec13a](https://gitlab.com/jhechtf/jhechtf/commit/57ec13acd67aa44a2e4cbca85d9d9f0f6a866dea))
- Merging some changes from different comps ([8e46bb3](https://gitlab.com/jhechtf/jhechtf/commit/8e46bb35677aa6aafd5a08449bfbf6750b289de7))
- Init ([876c8fc](https://gitlab.com/jhechtf/jhechtf/commit/876c8fc2512056ea5fe2bf02e97c0da1d7e99582))
